package com.sdakak;

import java.util.Arrays;
import java.util.List;
import junit.framework.TestCase;
import org.junit.Test;

public class InstallerTest extends TestCase {

  @Override
  protected void setUp() {
  }

  @Test
  public void testEarlyEnd() {
    Installer installer = new Installer();
    installer.readFile("src/test/resources/customInput1.txt");
    // if it reaches here there were no exceptions reading the file
    assertEquals(1, 1);
  }

  @Test
  public void testInstallAndRemoveUsingBigScenario() {
    Installer installer = new Installer();
    installer.readFile("src/test/resources/bigScenario.txt");
    List<String> expectedDeps = Arrays.asList("foo", "USB", "NETCARD");
    assertEquals(expectedDeps, installer.installedDependencies);
  }

  @Test
  public void testDontRemoveTransitiveDependencyIfItwasInstalledExplicitly() {
    Installer installer = new Installer();
    installer.readFile("src/test/resources/customInput3.txt");
    List<String> expectedDeps = Arrays.asList("NETCARD");
    assertEquals(expectedDeps, installer.installedDependencies);
  }

  @Test
  public void testReadFile() {
    Installer installer = new Installer();
    installer.readFile("src/test/resources/input.txt");
    // if it reaches here there were no exceptions reading the file
    assertEquals(1, 1);
  }

  @Test
  public void testInstall() {
    Installer installer = new Installer();
    installer.readFile("src/test/resources/testInstall1.txt");
    List<String> expectedDeps = Arrays
        .asList("NETCARD", "TELNET", "TCPIP", "foo", "BROWSER", "HTML", "DNS");
    assertEquals(expectedDeps, installer.installedDependencies);
  }

  @Test
  public void testRemove() {
    Installer installer = new Installer();
    installer.readFile("src/test/resources/testRemove1.txt");
    List<String> expectedDeps = Arrays.asList("NETCARD", "TCPIP", "foo", "DNS");
    assertEquals(expectedDeps, installer.installedDependencies);
  }

}
