package com.sdakak;

public class Dependency {

  private String name;
  private int requiredBy;

  public Dependency(String name) {
    this.name = name;
    this.requiredBy = 0;
  }

  public String getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Dependency that = (Dependency) o;

    return name.equals(that.name);
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }

  @Override
  public String toString() {
    return "Dependency{" +
        "name='" + name + '\'' +
        ", requiredBy=" + requiredBy +
        '}';
  }

  public void incrementRequiredBy() {
    requiredBy++;
  }

  public void decrementRequiredBy() {
    requiredBy--;
  }

}
