package com.sdakak;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Installer {

  private static final String DEPEND = "DEPEND";
  private static final String INSTALL = "INSTALL";
  private static final String REMOVE = "REMOVE";
  private static final String LIST = "LIST";
  private static final String END = "END";

  // visible for testing. Fix it with annotations
  Map<String, List<Dependency>> dependencies = new HashMap<>();
  Map<String, List<Dependency>> reversedDependencies = new HashMap<>();

  List<String> installedDependencies = new ArrayList<>();

  public void readFile(final String filename) {
    final File file = new File(filename);
    try {
      final Scanner sc = new Scanner(file);
      while (sc.hasNext()) {
        final String s = sc.nextLine();
        final String[] commandWithArgs = s.split(" ");
        final String[] args = Arrays.copyOfRange(commandWithArgs, 1, commandWithArgs.length);
        String command = commandWithArgs[0];
        if (command.equals(DEPEND)) {
          storeDependencies(args);
          System.out.println(s);
        } else if (command.equals(INSTALL)) {
          System.out.println(INSTALL + " " + commandWithArgs[1]);
          install(commandWithArgs[1]);
        } else if (command.equals(LIST)) {
          System.out.println(LIST);
          list();
        } else if (command.equals(REMOVE)) {
          System.out.println(REMOVE + " " + commandWithArgs[1]);
          remove(commandWithArgs[1]);
        } else if (command.equals(END)) {
          System.out.println(END);
          break;
        }
      }
    } catch (final FileNotFoundException e) {
      e.printStackTrace();
    }
  }

  private void remove(String program) {
    if (!installedDependencies.contains(program)) {
      System.out.println("    " + program + " is not installed");
    }

    final LinkedList<String> removeDeps = new LinkedList<>();
    removeDeps.addLast(program);

    // Remove till all transitive dependencies are processed
    while (!removeDeps.isEmpty()) {
      String dep = removeDeps.removeFirst();

      // From the total list of packages that need this package to be installed remove those that are not installed
      List<Dependency> requiredBy = reversedDependencies.get(dep);
      List<Dependency> installedRequirements = new ArrayList<Dependency>(requiredBy);
      for (Iterator<Dependency> iter = installedRequirements.listIterator(); iter.hasNext(); ) {
        Dependency d = iter.next();
        if (!installedDependencies.contains(d.getName())) {
          iter.remove();
        }
      }

      // If no one requires this package to be installed or the package only needs itself and we asked to explicitly remove it. Remove it
      if (requiredBy == null || installedRequirements.size() == 0 || (
          installedRequirements.size() == 1 && installedRequirements.get(0)
              .equals(new Dependency(program)))) {
        if (installedDependencies.contains(dep)) {
          System.out.println("    Removing " + dep);
          installedDependencies.remove(dep);
        }
      } else {
        if (program.equals(dep)) {
          System.out.println("    " + dep + " is still needed");
          break;
        }
      }

      // Add transitive dependencies as candidates for removal
      List<Dependency> transitiveDeps = dependencies.get(dep);
      if (transitiveDeps != null) {
        for (Dependency d : transitiveDeps) {
          removeDeps.addLast(d.getName());
        }
      }
    }
  }

  private void list() {
    for (String installed : installedDependencies) {
      System.out.println("     " + installed);
    }
  }

  private void install(final String program) {
    if (installedDependencies.contains(program)) {
      System.out.println("    " + program + " is already installed");
    }
    // Add software as dependency to itself if you install it by name. This prevents this dependency
    // from being transitively removed if nothing else depends on it
    final List<Dependency> currentRevDeps = reversedDependencies.get(program);
    if (currentRevDeps == null) {
      ArrayList<Dependency> newList = new ArrayList<>();
      newList.add(new Dependency(program));
      reversedDependencies.put(program, newList);
    } else if (!currentRevDeps.contains(new Dependency(program))) {
      currentRevDeps.add(new Dependency(program));
    }

    final LinkedList<String> installDeps = new LinkedList<>();
    installDeps.addLast(program);

    // install everything that is not installed transitively
    while (!installDeps.isEmpty()) {
      String dep = installDeps.removeFirst();
      if (!installedDependencies.contains(dep)) {
        installedDependencies.add(dep);
        System.out.println("    Installing " + dep);
      }
      List<Dependency> transitiveDeps = dependencies.get(dep);
      if (transitiveDeps != null) {
        for (Dependency d : transitiveDeps) {
          if (!installedDependencies.contains(d.getName())) {
            installDeps.addLast(d.getName());
          }
        }
      }
    }
  }

  private void storeDependencies(final String[] args) {
    // construct Dependencies from string deps entered
    final List<Dependency> deps = new ArrayList<>();
    final String[] depsEntered = Arrays.copyOfRange(args, 1, args.length);
    final String software = args[0];
    for (final String dep : depsEntered) {
      deps.add(new Dependency(dep));
    }
    // Add all the unique deps to the program's dependencies
    final List<Dependency> currentDeps = dependencies.get(software);
    if (currentDeps == null) {
      dependencies.put(software, deps);
    } else {
      for (final String dep : depsEntered) {
        if (!currentDeps.contains(new Dependency(dep))) {
          currentDeps.add(new Dependency(dep));
        }
      }
    }
    // construct reversedDeps used for removing stuff
    for (final String dep : depsEntered) {
      final List<Dependency> currentRevDeps = reversedDependencies.get(dep);
      if (currentRevDeps == null) {
        ArrayList<Dependency> newList = new ArrayList<>();
        newList.add(new Dependency(software));
        reversedDependencies.put(dep, newList);
      } else if (!currentRevDeps.contains(new Dependency(software))) {
        currentRevDeps.add(new Dependency(software));
      }
    }
//    System.out.println(dependencies);
  }


}
