package com.sdakak;

public class Main {
  public static void main (String args[]) {
    Installer installer = new Installer();
    if (args.length == 0) {
      System.out.println("Running with default file");
      System.out.println("Dir: " + System.getProperty("user.dir"));
      installer.readFile("src/main/resources/bigScenario.txt");
    } else {
      installer.readFile(args[0]);
    }
  }
}
